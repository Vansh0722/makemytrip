package Testing;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.lang3.ObjectUtils;
import org.junit.jupiter.api.Nested;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;


public class mmtTest {
    WebDriver driver = null;
    @Test
    public void homePage(){
        String expectedTitle = "MakeMyTrip - #1 Travel Website 50% OFF on Hotels, Flights & Holiday";
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.makemytrip.com");
        String actualTitle = driver.getTitle();

        Assert.assertEquals(expectedTitle,actualTitle);
        Assert.assertNotNull(expectedTitle,actualTitle);
    }

    @Test
    public void selectFlight(){
        WebElement search = driver.findElement(By.xpath("//a[contains(text(),'Search')]"));
        search.click();
        String expectedClass = "MakeMyTrip";
        String actualClass = driver.getTitle();
        System.out.println(actualClass);
        Assert.assertEquals(expectedClass,actualClass);
    }

}
