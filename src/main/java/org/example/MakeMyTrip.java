package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.List;
import java.util.Set;

public class MakeMyTrip {
    WebDriver driver;
    @Test
    public void invokeBrowser(){
            driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
            driver.manage().window().maximize();
            driver.get("https://www.makemytrip.com");
            selectDateAndDestination();
    }


    @Test
    public void selectDateAndDestination(){
            //CODE STARTS..
            //REMOVE ADDS..
//            driver.switchTo().frame(driver.findElement(By.id("webklipper-publisher-widget-container-notification-frame")));
//            driver.findElement(By.id("webklipper-publisher-widget-container-notification-close-div")).click();
//            driver.switchTo().defaultContent();
            driver.findElement(By.cssSelector("#root > div > div.minContainer > div > main > div.langCard.fixedCard.bounceAni > span")).click();
            //SEARCH AND SELECT YOUR CITY
            WebElement selectCity1 = driver.findElement(By.xpath("//input[@id='fromCity']"));
            selectCity1.click();
            new WebDriverWait(driver, Duration.ofSeconds(8)).until(
                    ExpectedConditions.visibilityOf(driver.findElement(By.xpath(
                            "//p[@class='font14 appendBottom5 blackText']"))));
            WebElement citySelect1 = driver.findElement(By.xpath("//p[@class='font14 appendBottom5 blackText']"));
            citySelect1.click();
            //SEARCH AND YOUR DESTINATION CITY..
            WebElement selectCity2 = driver.findElement(By.className("react-autosuggest__input"));
            selectCity2.click();
             List<WebElement> cityNames = driver.findElements(By.xpath("//div[contains(@class,'react-autosuggest__section-container')]//li"));
             cityNames.get(5).click();

            //SELECTING THE DATE..
            String monthYearValue = driver.findElement(By.cssSelector("body.desktop.in.webp:nth-child(2) div.bgGradient.webpSupport div.minContainer div.widgetSection.appendBottom40.primaryTraveler div.fsw.widgetOpen div.fsw_inner.returnPersuasion:nth-child(1) div.fsw_inputBox.dates.inactiveWidget.activeWidget:nth-child(4) div.datePickerContainer:nth-child(2) div.flightCalOverlay div.dayPickerFlightWrap div.RangeExample.oneWay div.DayPicker div.DayPicker-wrapper div.DayPicker-Months div.DayPicker-Month:nth-child(1) > div.DayPicker-Caption")).getText();
            String month = monthYearValue.split(" ")[0].trim();
            String year = monthYearValue.split(" ")[1].trim();
            while(!(month.equals("September") && year.equals("2022"))){
                driver.findElement(By.cssSelector("body.desktop.in.webp:nth-child(2) div.bgGradient.webpSupport div.minContainer div.widgetSection.appendBottom40.primaryTraveler div.fsw.widgetOpen div.fsw_inner.returnPersuasion:nth-child(1) div.fsw_inputBox.dates.inactiveWidget.activeWidget:nth-child(4) div.datePickerContainer:nth-child(2) div.flightCalOverlay div.dayPickerFlightWrap div.RangeExample.oneWay div.DayPicker div.DayPicker-wrapper div.DayPicker-NavBar > span.DayPicker-NavButton.DayPicker-NavButton--next:nth-child(2)")).click();
                monthYearValue = driver.findElement(By.cssSelector("body.desktop.in.webp:nth-child(2) div.bgGradient.webpSupport div.minContainer div.widgetSection.appendBottom40.primaryTraveler div.fsw.widgetOpen div.fsw_inner.returnPersuasion:nth-child(1) div.fsw_inputBox.dates.inactiveWidget.activeWidget:nth-child(4) div.datePickerContainer:nth-child(2) div.flightCalOverlay div.dayPickerFlightWrap div.RangeExample.oneWay div.DayPicker div.DayPicker-wrapper div.DayPicker-Months div.DayPicker-Month:nth-child(1) > div.DayPicker-Caption")).getText();
                month = monthYearValue.split(" ")[0].trim();
                year = monthYearValue.split(" ")[1].trim();
            }
            WebElement date = driver.findElement(By.cssSelector("body.desktop.in.webp:nth-child(2) div.bgGradient.webpSupport div.minContainer div.widgetSection.appendBottom40.primaryTraveler div.fsw.widgetOpen div.fsw_inner.returnPersuasion:nth-child(1) div.fsw_inputBox.dates.inactiveWidget.activeWidget:nth-child(4) div.datePickerContainer:nth-child(2) div.flightCalOverlay div.dayPickerFlightWrap div.RangeExample.oneWay div.DayPicker div.DayPicker-wrapper div.DayPicker-Months div.DayPicker-Month:nth-child(1) div.DayPicker-Body div.DayPicker-Week:nth-child(2) > div.DayPicker-Day:nth-child(5)"));
            date.click();
            //CONTINUE WITH YOUR TIME AND DATE..
            WebElement search = driver.findElement(By.xpath("//a[contains(text(),'Search')]"));
            search.click();
            selectTimeAndFlight();

        }
    @Test
    public void selectTimeAndFlight(){
        //BANNER HANDLE..
        WebElement bannerHandle = driver.findElement(By.className("overlayCrossIcon"));
        bannerHandle.click();
        //USING FILTER FOR SELECT YOUR TIME AND FLIGHT..
        WebElement filter = driver.findElement(By.cssSelector("div.flightBody div.flightsContainer.makeFlex.spaceBetween div.listingLhs div.filterWrapper:nth-child(2) div.filtersOuter:nth-child(1) div:nth-child(2) label.makeFlex.spaceBtwCenter.appendBottom12.pointer:nth-child(2) div.makeFlex.hrtlCenter.flexOne.gap-x-10:nth-child(1) span.makeFlex.hrtlCenter > span.filterName"));
        filter.click();
        //SEE PRICES..
//        WebElement viewPrice = driver.findElement(By.xpath("//p[text()='IndiGo']/../../..//span[text()='09:00']/../../../../../../../..//button/span[text()='View Prices']"));
//        viewPrice.click();
        List<WebElement> flights = driver.findElements(By.xpath("//div[@id='premEcon']//div"));
            WebElement flight =  flights.get(4);
            WebElement viewPrices = flight.findElement(By.xpath("//button/span[text()='View Prices']"));
            viewPrices.click();
        //BOOK NOW..
        WebElement bookNow = driver.findElement(By.xpath("//span[text()='Cancellation Fee Starting ₹ 3,000']/../../..//button[text()='Book Now']"));
        bookNow.click();
        travellerDetails();
    }

    @Test
    public void travellerDetails() {
        //PROCEED NEXT PAGE STEPS AND PROCEED TO BOOKING..
        String parentHandle = driver.getWindowHandle();
        Set<String> handles = driver.getWindowHandles();
        for (String handle : handles) {
            if (!handle.equals(parentHandle)) {
                driver.switchTo().window(handle);
                new WebDriverWait(driver, Duration.ofSeconds(8)).until(
                        ExpectedConditions.visibilityOf(driver.findElement(By.xpath(
                                "//b[contains(text(),'Yes, Secure my trip.')]/../..//span[@class='outer']"))));
                //Insurance for your trip..
                WebElement secureMyTrip = driver.findElement(By.xpath("//b[contains(text(),'Yes, Secure my trip.')]/../..//span[@class='outer']"));
                secureMyTrip.click();
                //FILL TRAVELLER DETAILS..
                new WebDriverWait(driver, Duration.ofSeconds(8)).until(ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(text(),'+ ADD NEW ADULT')]")));
                WebElement travellerDetails = driver.findElement(By.xpath("//button[contains(text(),'+ ADD NEW ADULT')]"));
                travellerDetails.click();
                WebElement enterFirstName = driver.findElement(By.cssSelector("div.reviewTravellerAddons div.flightBody div.flightsContainer.footerSpace div.makeFlex.spaceBetween div.pageLeftConainer div.oneCard-element:nth-child(5) div.componentContainer.appendTop15.appendBottom20 div.travellerWrapper:nth-child(1) div.appendBottom20 div.adultListWrapper div.AdultFormWrapper.collapse.show div.adultDetailsForm div.adultDetailsInnner div.adultItemRow.appendBottom15 div.adultItem:nth-child(1) div.relative > input.tvlrInput"));
                enterFirstName.sendKeys("Vansh");
                WebElement enterLastName = driver.findElement(By.cssSelector("div.reviewTravellerAddons div.flightBody div.flightsContainer.footerSpace div.makeFlex.spaceBetween div.pageLeftConainer div.oneCard-element:nth-child(5) div.componentContainer.appendTop15.appendBottom20 div.travellerWrapper:nth-child(1) div.appendBottom20 div.adultListWrapper div.AdultFormWrapper.collapse.show div.adultDetailsForm div.adultDetailsInnner div.adultItemRow.appendBottom15 div.adultItem:nth-child(2) div.relative > input.tvlrInput"));
                enterLastName.sendKeys("Chaudhary");
                WebElement gender = driver.findElement(By.xpath("//label[@tabindex='0']"));
                gender.click();
                WebElement mobile = driver.findElement(By.xpath("//input[@placeholder='Mobile No']"));
                mobile.sendKeys("9058885506");
                WebElement email = driver.findElement(By.xpath("//input[@placeholder='Email']"));
                String emailId = "vanshchaudhary@gmail.com";
                email.sendKeys(emailId);
                List<WebElement> emailIds = driver.findElements(By.className("emailId"));
                if (!emailIds.isEmpty()) {
                    for (WebElement capturedEmailId : emailIds) {
                        if (capturedEmailId.getText().equals(emailId)) {
                            capturedEmailId.click();
                            break;
                        }
                    }
                }
                //SUBMIT WHOLE DETAILS..
                new WebDriverWait(driver, Duration.ofSeconds(8)).until(
                        ExpectedConditions.visibilityOf(driver.findElement(By.xpath(
                                "//div[contains(@class,'spaceBetween')]//button[text()='Continue']"))));
                WebElement saveSearch = driver.findElement(By.xpath("//div[contains(@class,'spaceBetween')]//button[text()='Continue']"));
                saveSearch.click();
                WebElement confirm = driver.findElement(By.xpath("//button[contains(text(),'CONFIRM')]"));
                confirm.click();
                chooseTheSeatsAndProceedToPayment();
            }
        }
    }

    @Test
    public void chooseTheSeatsAndProceedToPayment(){
        //CHOOSING SEATS..
                new WebDriverWait(driver, Duration.ofSeconds(8)).until(
                        ExpectedConditions.visibilityOf(driver.findElement(By.xpath(
                                "//span[contains(text(),'No, Let Me Choose')]"))));
                WebElement selectSeat = driver.findElement(By.xpath("//span[contains(text(),'No, Let Me Choose')]"));
                selectSeat.click();
                new WebDriverWait(driver, Duration.ofSeconds(8)).until(
                        ExpectedConditions.visibilityOf(driver.findElement(By.xpath(
                                "//div[@class='seatRow'][18]//div[@class='seatCol'][2]"))));
                WebElement chooseSeat = driver.findElement(By.xpath("//div[@class='seatRow'][18]//div[@class='seatCol'][2]"));
                chooseSeat.click();
                //CONFIRM YOUR SEAT..
                new WebDriverWait(driver, Duration.ofSeconds(8)).until(
                        ExpectedConditions.visibilityOf(driver.findElement(By.xpath(
                                "//button[contains(text(),'Continue')]"))));
                WebElement continueAfterSeat = driver.findElement(By.xpath("//button[contains(text(),'Continue')]"));
                continueAfterSeat.click();
                WebElement continueAfterSeat2 = driver.findElement(By.xpath("//button[contains(text(),'Continue')]"));
                continueAfterSeat2.click();
                WebElement continueAnyWay = driver.findElement(By.xpath("//button[contains(text(),'CONTINUE ANYWAY')]"));
                continueAnyWay.click();
                //PROCEED TO PAY..
                new WebDriverWait(driver, Duration.ofSeconds(8)).until(
                        ExpectedConditions.visibilityOf(driver.findElement(By.xpath(
                                "//button[contains(text(),'Proceed to pay')]"))));
                WebElement ProceedToPay = driver.findElement(By.xpath("//button[contains(text(),'Proceed to pay')]"));
                ProceedToPay.click();
                WebElement LogIn = driver.findElement(By.xpath("//button[contains(text(),'login now')]"));
                LogIn.click();

    }



    public static void main (String[]args) throws InterruptedException {
                MakeMyTrip webPage = new MakeMyTrip();
                System.setProperty("webdriver.chrome.driver", "/home/vansh/Desktop/java/seleniumProjects/chromedriver");
                webPage.driver = new ChromeDriver();
                webPage.invokeBrowser();
    }
}


